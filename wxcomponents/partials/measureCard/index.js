const app = getApp();
Component({
  properties: {
    showCount: {
      type: JSON,
    },
    productData: {
      type: JSON,
    },
    bindway: {
      type: JSON,
    },
    byNowParams:{
      type: JSON,
    },
    pintuanParams:{
      type: JSON,
    }
  },
  data: {
    someData: {},
    sysWidth:"",
    setting:null,
    measurementJson: null,
    MeasureParams:[],
  },
  ready:function(){
   let that=this;
   console.log("measureCard",that.data.showCount,that.data.byNowParams,that.data.productData)
   that.setData({
     setting: app.globalData.setting
   })
   if(that.data.productData){
      that.chooseMeasureItem()
   }
  },
  methods: {
      closeZhezhao: function () {
        let that=this;
        that.setData({ showCount: false })
        that.triggerEvent('closeCard', {state:'success'})
      },
      /* 立即购买 */
      buyNow: function (e) {
        let that=this;
        app.globalData.consoleFun("=====productList组件-buyNow=====",[e])
        if (!app.globalData.checkShopOpenTime()) {
          return
        }
        if (!app.globalData.checkIfLogin()) {
          return
        }
        let bindway;
        if (e.currentTarget.dataset.way) {
          bindway = e.currentTarget.dataset.way
        } else {
          bindway = that.data.bindway
        }
        app.globalData.consoleFun("=====productList组件-bindway=====",[bindway])
        that.setData({ haveMeasuresState: true })
        that.setData({ selectTypeData: that.data.selectTypeData })
        app.globalData.consoleFun("=====productList组件-addtocart=====")
        if(bindway=='cart'){
          that.addtocart()
        }else{
          that.createOrder22(that.data.byNowParams);
        }
      },
      /* 创建订单 */
      createOrder22: function (o) {
        console.log('========createOrder22======', o);
        let that = this;
        let productInfo = that.data.productData.productInfo;
        let params = Object.assign({}, params, that.data.byNowParams, that.data.pintuanParams);
        console.log("=======", that.data.byNowParams, that.data.pintuanParams);
        app.globalData.showToastLoading('loading', true);
        app.globalData.createOrder(that.data.byNowParams, that.data.pintuanParams);
      },
      // 这里是一个自定义方法
      // readyAddCar: function (data) {
      //   let that = this;
      //   app.globalData.consoleFun("=====productList组件-readyAddCar=====",[ data, that.data.byNowParams])
      //   let productInfo =data.productInfo;
      //   let productId = productInfo.productId || productInfo.id
      //   app.globalData.consoleFun("=====productList组件-productId=====",[productId])
      //   that.get_product_measure(productId)
      // },
      //获取规格价格参数
      // get_product_measure: function (id) {
      //   let that=this;
      //   let productId = id
      //   let customIndex = app.globalData.AddClientUrl("/get_product_measures.html", { productId: productId})
      //   wx.request({
      //     url: customIndex.url,
      //     header: app.globalData.header,
      //     success: function (res) {
      //       app.globalData.consoleFun("=====productList组件-success=====",[res.data])
      //       let measures = res.data;
      //       that.data.productData.measures = measures
      //       that.setData({ productData: that.data.productData})
      //       if (measures.length == 0) {
      //         that.addtocart()
      //       } else {
      //         that.setData({ bindway: "cart", showCount: true })
      //         let info = that.data.productData.productInfo;
      //         app.globalData.consoleFun("=====productList组件-info=====",[info])
      //         that.data.byNowParams.productId = info.id
      //         that.data.byNowParams.shopId = info.belongShopId || info.shopId
      //         that.data.byNowParams.orderType = 0
      //         that.setData({ byNowParams: that.data.byNowParams })
      //         app.globalData.consoleFun("=====productList组件-byNowParams=====",[that.data.byNowParams])
      //         that.chooseMeasureItem()
      //       }
      //     },
      //     fail: function (res) {
      //       app.globalData.consoleFun("=====productList组件-fail=====")
      //       app.globalData.loadFail()
      //     },
      //     complete: function () {
      //     },
      //   })
      // },
      /* 初始化 选规格 */
      chooseMeasureItem: function (event) {
        app.globalData.consoleFun("=====productList组件-初始化规格参数=====",[event])
        let that=this;
        let productData = that.data.productData
        let focusProduct = productData
        let selectTypeData = []
        for (let i = 0; i < focusProduct.measures.length; i++) {
          focusProduct.measures[i].checkedMeasureItem = 0
          let param = {}
          let selectTypeDataItem = {}
          param.name = focusProduct.measures[i].name
          param.value = focusProduct.measures[i].productAssignMeasure[0].id
          selectTypeDataItem.type = focusProduct.measures[i].name
          selectTypeDataItem.value = focusProduct.measures[i].productAssignMeasure[0].measureName
          app.globalData.consoleFun("=====productList组件-param=====",[param])
          that.data.MeasureParams.push(param)
          selectTypeData.push(selectTypeDataItem)
        }
        that.data.selectTypeData = selectTypeData
        app.globalData.consoleFun("=====productList组件-selectTypeData=====",[that.data.selectTypeData])
        that.setData({
          productData: focusProduct
        })
        app.globalData.consoleFun("=====productList组件-MeasureParams=====",[that.data.MeasureParams])
        that.get_measure_cartesion()
      },
      //选择规格小巷的---显示
      radioChange: function (e) {
        let that = this
        let flag = false;
        app.globalData.consoleFun("=====productList组件-radioChange=====",[e])
        app.globalData.consoleFun("=====productList组件-selectTypeData=====",[that.data.selectTypeData])
        if (that.data.selectTypeData) {
          for (let i = 0; i < that.data.selectTypeData.length; i++) {
            if (e.currentTarget.dataset.type == that.data.selectTypeData[i].type) {
              that.data.selectTypeData.splice(i, 1, e.currentTarget.dataset)
              flag = true;
            }
          }
          if (!flag) {
            that.data.selectTypeData.splice(that.data.selectTypeData.length, 1, e.currentTarget.dataset)
            flag = false;
          }
        } else {
          that.data.selectTypeData = [];
          that.data.selectTypeData.splice(0, 1, e.currentTarget.dataset)
          that.setData({ selectTypeData: that.data.selectTypeData })
        }
        app.globalData.consoleFun("=====productList组件-selectTypeData=====",[that.data.selectTypeData])
        let index = e.currentTarget.dataset.index
        let indexJson = app.globalData.getSpaceStr(index, '-')
        let focusItem = that.data.productData
        focusItem.measures[indexJson.str1].checkedMeasureItem = indexJson.str2
        that.setData({ productData: focusItem })
      },
      //选择规格小巷---获取数据
      chooseMeasure: function (e) {
        app.globalData.consoleFun("=====productList组件-chooseMeasure=====",[e.detail.value])
        let chooseMeasureJson = app.globalData.getSpaceStr(e.detail.value, '-')
        app.globalData.consoleFun("=====productList组件-chooseMeasureJson=====",[chooseMeasureJson])
        for (let i = 0; i < this.data.MeasureParams.length; i++) {
          if (this.data.MeasureParams[i].name == chooseMeasureJson.str1) {
            this.data.MeasureParams[i].value = chooseMeasureJson.str2
          }
        }
        this.get_measure_cartesion()
      },
      //获取规格价格参数
      get_measure_cartesion: function () {
        let that = this
        that.setData({ measurementJson: { waitDataState: false } })
        let productId = that.data.productData.productInfo.productId
        let postStr = ''
        if (that.data.MeasureParams.length == 0) {
          that.data.byNowParams.cartesianId = '0'
          that.data.byNowParams.itemCount = that.data.productData.productInfo.minSaleCount
          that.setData({ minCount: that.data.byNowParams.itemCount,byNowParams: that.data.byNowParams, measurementJson: { waitDataState: true } })
          return
        }
        for (let i = 0; i < that.data.MeasureParams.length; i++) {
          postStr += that.data.MeasureParams[i].value + ','
        }
        let param = {}
        param.productId = productId
        param.measureIds = postStr
        app.globalData.consoleFun("=====productList组件-get_measure_cartesion=====",[postStr])
        let customIndex = app.globalData.AddClientUrl("/get_measure_cartesion.html", param)
        wx.request({
          url: customIndex.url,
          header: app.globalData.header,
          success: function (res) {
            app.globalData.consoleFun("=====productList组件-measurementJson=====",[res.data])
            that.data.byNowParams.cartesianId = res.data.id
            that.setData({
              measurementJson: res.data
            })
            that.data.measurementJson.waitDataState = true
            that.setData({ measurementJson: that.data.measurementJson })
            that.data.byNowParams.itemCount = that.data.measurementJson.minSaleCount
            that.setData({ minCount: that.data.byNowParams.itemCount})
            that.setData({ byNowParams: that.data.byNowParams })
          },
          fail: function (res) {
            app.globalData.consoleFun("=====productList组件-fail=====")
            app.globalData.loadFail()
          },
          complete: function () {
          },
        })
      },
      subNum: function () {
        let that=this;
        console.log("this.data.measurementJson",that.data.measurementJson)
        if (that.data.measurementJson&&that.data.measurementJson.id) {
          that.setData({ minCount: that.data.measurementJson.minSaleCount })
        } else {
          that.setData({ minCount: 1 })
        }
        if (that.data.byNowParams.itemCount == that.data.minCount) {
          return
        }
        that.data.byNowParams.itemCount--;
        that.setData({ byNowParams: that.data.byNowParams })
      },
      addNum: function () {
        this.data.byNowParams.itemCount++;
        this.setData({ byNowParams: this.data.byNowParams })
      },
      addtocart: function () {
        app.globalData.consoleFun("=====productList组件-addtocart=====")
        if (!app.globalData.checkIfLogin()) {
          return
        }
        var params = {
          cartesianId: '',
          productId: '',
          shopId: '',
          count: '',
          type: '',
        }
        if (this.data.productData.measures.length == 0) {
          params.cartesianId = '0'
        }
        else {
          params.cartesianId = this.data.measurementJson.id
        }
        let productId = this.data.productData.productInfo.productId || this.data.productData.productInfo.id
        app.globalData.consoleFun("=====productList组件-productId=====",[productId])
        params.productId = productId
        params.shopId = this.data.productData.productInfo.belongShopId || this.data.productData.productInfo.shopId
        params.count = this.data.byNowParams.itemCount
        params.type = 'add'
        app.globalData.consoleFun("=====productList组件-params=====",[params])
        this.postParams(params)
      },
      postParams: function (data) {
        app.globalData.consoleFun("=====productList组件-postParams=====")
        var that = this
        var customIndex = app.globalData.AddClientUrl("/change_shopping_car_item.html", data, 'post')
        wx.request({
          url: customIndex.url,
          data: customIndex.params,
          header: app.globalData.headerPost,
          method: 'POST',
          success: function (res) {
            let resEventData = res.data
            that.triggerEvent('resEvent', { resEventData}, {})
            app.globalData.consoleFun("=====productList组件-change_shopping_car_item=====",[res.data])
            wx.hideLoading()
            if (that.data.bindway == 'cart') {
              that.setData({ showCount: false })
            }
            if (data.productId == 0) {
              app.globalData.consoleFun("=====productList组件-购物车里面的商品数量=====")
              that.setData({
                countGood: res.data.totalCarItemCount
              })
            } else {
              if (res.data.productId && res.data.productId != 0) {
                if (data.count == 0) {
                  app.globalData.consoleFun("=====productList组件-通过加入购物车来确定购物车里面的商品数量=====")
                } else {
                  wx.showToast({
                    title: '加入购物车成功',
                  })
                }
                if (!!res.data.totalCarItemCount || res.data.totalCarItemCount == 0) {
                  that.setData({ countGood: res.data.totalCarItemCount })
                }
              } else {
                wx.showToast({
                  title: res.data.errMsg,
                  image: '/wxcomponents/images/icons/tip.png',
                  duration: 2000
                })
              }
            }
            try {
              app.globalData.carChangeNotify(res.data);
            } catch (e) { }
            app.globalData.consoleFun("=====productList组件-加入购物车动作=====")
            that.triggerEvent('closeCard', {state:'success'})
          },
          fail: function (res) {
            wx.hideLoading()
            app.globalData.loadFail()
          }
        })
      },
      //提交规格产品
      submitMeasure: function (id) {
        var that = this
        let focusProduct = this.data.MeasureItem
        let measurementJson = this.data.measurementJson
        let data = {}
        data.cartesianId = measurementJson.id
        data.productId = focusProduct.id
        data.shopId = focusProduct.belongShopId
        data.count = 1
        data.type = 'add'
        var customIndex = app.globalData.AddClientUrl("/change_shopping_car_item.html", data, 'post')
        wx.request({
          url: customIndex.url,
          data: customIndex.params,
          header: app.globalData.headerPost,
          method: 'POST',
          success: function (res) {
            app.globalData.consoleFun("=====productList组件-add=====",[res.data])
            that.setData({ showGuigeType: false })
          },
          fail: function (res) {
            app.globalData.loadFail()
          },
          complete: function () {
            wx.hideLoading()
          }
        })
      },
      closeGuigeZhezhao: function () {
        this.setData({ showGuigeType: false })
        this.data.MeasureParams = []
      },
      tolinkUrl: function (e) {
        app.globalData.consoleFun("=====productList组件-tolinkUrl=====",[e.currentTarget.dataset.info])
        let productData = e.currentTarget.dataset.info
        let link="";
        if (productData.productType==6){
          link = "ticket_detail.html?productId=" + productData.id; 
        }else{
          link = "product_detail.html?productId=" + productData.id; 
        }
        app.globalData.linkEvent(link);
      }
  },
})