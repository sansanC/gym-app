const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
    }
  },
  data: {
    item: {},
    color:'#888',
    tipText:"",
    count:152366544,
    setting:null,
    defaultColor:'',
    secondColor:''
  },
  
   ready:function(){
     let that=this;
     console.log("component-price-group",that.data.data)
     that.setData({
       item: that.data.data,
       defaultColor: app.globalData.setting.platformSetting.defaultColor,
       secondColor: app.globalData.setting.platformSetting.secondColor,
       setting: app.globalData.setting
     })
   },
  methods: {
    tolinkUrl: function (event) {
      console.log(event.currentTarget.dataset.link)
      app.globalData.linkEvent(event.currentTarget.dataset.link);

    }
  },
})