const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {}
  },
  ready: function () {
      let that=this;
    // app.globalData.consoleFun("=====newsList组件-data=====",[that.data.data])
  },
  methods: {
    tolinkUrl: function (event) {
      app.globalData.consoleFun("=====newsList组件-id=====",[event.currentTarget.dataset.id])
      let itemData=event.currentTarget.dataset.item;
      let linkUrl='news_detail.html?id=' + itemData.id;
      if(itemData.linkUrl){
          linkUrl=itemData.linkUrl
      }
	  
	  app.globalData.linkEvent(linkUrl)
    }
  },
})