const app = getApp();
var WxParse = require('../../wxParse/wxParse.js');
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    page:"1",
    shops:[],
    journey:[],//公里数
  },
  ready:function(){
    this.getData();
    if (app.globalData.setting.platformSetting.defaultColor && app.globalData.setting.platformSetting.defaultColor !=""){
      app.globalData.consoleFun("=====nearShop组件-defaultColor=====",[this.data.data,app.globalData.setting.platformSetting.defaultColor])
      // 有默认色
      this.setData({
        defaultColor: app.globalData.setting.platformSetting.defaultColor
      })
    }
    else{
      // 没有默认色
      this.setData({
        defaultColor: app.globalData.setting.platformSetting.defaultColor
      })
    }
  },
  methods: {
      toProlinkUrl: function (e) {
        app.globalData.consoleFun("=====productList组件-tolinkUrl=====",[e.currentTarget.dataset.info])
        let productData = e.currentTarget.dataset.info
        let link="";
        if (productData.productType==6){
          link = "ticket_detail.html?productId=" + productData.id; 
        }else{
          link = "product_detail.html?productId=" + productData.id; 
        }
        app.globalData.linkEvent(link);
      },
      // 定位
      clickCatch: function (e) {
        console.log("===clickCatch===",e);
        let itemData=e.currentTarget.dataset.item
        let latitude = itemData.latitude;
        let longitude = itemData.longitude;
        let name = itemData.shopName;
        let address = itemData.shopLocation;
        wx.openLocation({
          latitude: latitude,
          longitude: longitude,
          scale: 12,
          name: name,
          address: address
        });
      },
    tolinkUrl: function (e) {
      app.globalData.consoleFun("=====nearShop组件-link=====",[e.currentTarget.dataset.link])
      let linkUrl = e.currentTarget.dataset.link
      app.globalData.linkEvent(linkUrl)
    },
    getData:function(){
      let that = this;
      let shopName = app.globalData.setting.platformSetting.defaultShopBean.account.shopName
      wx.getLocation({
        type: 'gcj02',
        success: function (res) {
          let latitude = res.latitude
          let longitude = res.longitude
          app.globalData.consoleFun("=====nearShop组件-getLocation=====",[longitude,latitude,that.data.page])
          let pageParam = { 
            "longitude": longitude,
            "latitude": latitude,
            "page": that.data.page,
            "pageObjectType":that.data.data.pageObjectType||0,
            "pageObjectId":that.data.data.pageObjectId||0
          }
          if(that.data.data.jsonData.shopTypeId){
              pageParam=Object.assign({},pageParam,{ "shopTypeId":that.data.data.jsonData.shopTypeId})
          }
          app.globalData.consoleFun("=====nearShop组件-pageParam=====",[pageParam])
          let customIndex = app.globalData.AddClientUrl("/more_near_shops.html", pageParam, 'get', 1)
          app.globalData.showToastLoading('loading', true)
          wx.request({
            url: customIndex.url,
            header: app.globalData.header,
            method: 'GET',
            success: function (res) {
              that.setData({
                shops: res.data.relateObj.result
              })
              app.globalData.consoleFun("=====nearShop组件-success=====",[res,res.data.relateObj.result])
              // 店铺标签是带【】的字符串需要改
              let shops = res.data.relateObj.result;
              let tagArray=[];
              for (let j = 0; j < shops.length; j++) {
                // 获取公里数
                shops[j].distance = app.globalData.getDistance(latitude, longitude, shops[j].latitude, shops[j].longitude)
                if (shops[j].shopTag) {
                  tagArray = shops[j].shopTag.slice(1, -1).split("][")
                  shops[j].tagArray = tagArray;
                }
              }
              that.setData({shops: shops})
              if (res.data.errcode < 0) {
                app.globalData.consoleFun("=====nearShop组件-fail=====",[res.data.errMsg])
              }
              else {
                wx.hideLoading()
              }
            },
            fail: function (res) {
              wx.hideLoading()
              app.globalData.loadFail()
            }
          })
        }
      })
    },
  },
})